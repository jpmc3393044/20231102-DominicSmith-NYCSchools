20231102-DominicSmith-NYCSchools

- App utilizes Swift and SwiftUI
- Displays a list of NYC Highschools. 
- Tapping on a view will show the high school in detail with the average SAT Scores listed. 
- Users can get directions, call the school, and go straight to the website from the school details page. 
- App should work right after cloning. No non-apple 3rd party libraries were utilized for this project.
- App utilizes caching for faster loading. On first load, app queries data, then caches for local query later.
- App is optimized for both dark and light modes.
- App is optimized for both portrait and landscape modes. 
- See wiki for screenshots
