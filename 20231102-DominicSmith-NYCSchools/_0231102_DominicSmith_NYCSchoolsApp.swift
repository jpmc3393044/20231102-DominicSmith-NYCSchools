//
//  _0231102_DominicSmith_NYCSchoolsApp.swift
//  20231102-DominicSmith-NYCSchools
//
//  Created by Dominic  Smith on 11/2/23.
//

import SwiftUI

@main
struct _0231102_DominicSmith_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
