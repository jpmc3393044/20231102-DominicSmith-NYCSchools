//
//  ContentView.swift
//  20231102-DominicSmith-NYCSchools
//
//  Created by Dominic  Smith on 11/2/23.
//
//  future improvements could include a search bar that narrows down schools..
//  by name, borough, average sat score types...
//  anything that would help a student find a school...
//  that fits their criteria for school selection

import SwiftUI

struct ContentView: View {
    var body: some View {
        HighSchoolListView()
    }
}

struct HighSchoolListView: View {
    @StateObject var highschoolViewModel = HighschoolViewModel()
    
    var body: some View {
        NavigationStack {
            Text("NYC Schools")
                .font(.title)
            switch highschoolViewModel.loadingState {
            case .loading:
                ProgressView()
            case .loaded:
                List(highschoolViewModel.schools) { school in
                    NavigationLink(destination: SchoolDetailView(school: school)) {
                        previewSchoolInfo(school: school)
                    }
                }
            case .failed:
                Text("Unable to fetch Schools")
                Button("Try Again") {
                    Task {
                        await highschoolViewModel.fetchSchools()
                    }
                }
            }
        }
        .onAppear {
            Task {
                await highschoolViewModel.fetchSchools()
            }
        }
    }
}

struct previewSchoolInfo: View {
    let school: HighSchool
    @State private var shouldLimitLine = true
    @State private var isExpanded = false
    var body: some View {
        VStack(alignment: .leading) {
            Text(school.school_name ?? "")
                .bold()
            let address = buildAddressFromLocationComponents(school)
            Text("\(address)")
                .foregroundColor(.secondary)
        }
    }
    
    func buildAddressFromLocationComponents(_ school: HighSchool) -> String {
        var address = ""
        address = "\(school.primary_address_line_1 ?? ""), \(school.city ?? ""), \(school.state_code ?? ""), \(school.zip ?? "")"
        return address
    }
}

struct SchoolDetailView: View {
    @StateObject var highschoolViewModel = HighschoolViewModel()

    let school: HighSchool

    var body: some View {
        List {
            VStack(alignment: .leading) {
                previewSchoolInfo(school: school)
                Divider()
                //for this HStack chunk, could also do..
                //a forEach that iterates through each HStack item
                HStack {
                    if let longitude = school.longitude,
                       let latitude = school.latitude,
                        let url =  URL(string: "https://maps.apple.com/?ll=\(latitude),\(longitude)") {
                        Button(action: {
                            UIApplication.shared.open(url)
                        }) {
                            HStack {
                                Image(systemName: "mappin")
                                Text("Directions")
                            }
                        }
                    }
                    
                    if let phone_number = school.phone_number,
                       let phone_number_link = URL(string: "tel:\(phone_number)") {
                        Button(action: {
                            UIApplication.shared.open(phone_number_link)
                        }) {
                            HStack {
                                Image(systemName: "phone.fill")
                                Text("Call")
                            }
                        }
                    }
                    
                    if let website = formatWebsite(school),
                        let websiteURL = URL(string: "\(website)") {
                        Button(action: {
                            UIApplication.shared.open(websiteURL)
                        }) {
                            HStack {
                                Image(systemName: "globe")
                                Text("Link")
                            }
                        }
                     }
                }
                .buttonStyle(.bordered)
                Divider()
                Text("SAT Score Averages")
                    .bold()
                switch highschoolViewModel.loadingState {
                case .loading:
                    ProgressView()
                case .loaded:
                    Text("Number of Test Takers: \(highschoolViewModel.satScores?.num_of_sat_test_takers ?? "N/A")")
                    Text("Math: \(highschoolViewModel.satScores?.sat_math_avg_score ?? "N/A")")
                    Text("Writing: \(highschoolViewModel.satScores?.sat_writing_avg_score ?? "N/A")")
                    Text("Reading: \(highschoolViewModel.satScores?.sat_critical_reading_avg_score ?? "N/A")")
                case .failed:
                    Text("N/A")
                        .padding([.bottom], 5)
                }
                Divider()
                Text("Overview")
                    .bold()
                Text("\(school.overview_paragraph ?? "")")
            }
        }
        .onAppear {
            Task {
                await highschoolViewModel.fetchSatScores(school.school_name ?? "")
            }
        }
    }
    
    //the websites aren't returned as structured URLs that the web browser recognizes
    //this method formats the URLs so that they can be opened in Safari from the app
    func formatWebsite(_ school: HighSchool) -> String? {
        guard let website = school.website else { return nil }
        if website.starts(with: "https") ||
            website.starts(with: "http") {
            return website
        }
        return "https://\(website)"
    }
}

#Preview {
    ContentView()
}
