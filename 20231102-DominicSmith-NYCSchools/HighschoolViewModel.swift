//
//  HighschoolViewModel.swift
//  20231102-DominicSmith-NYCSchools
//
//  Created by Dominic  Smith on 11/2/23.
//

import Foundation
import SwiftUI

class HighschoolViewModel: ObservableObject {
    @Published var schools: [HighSchool] = []
    @Published var loadingState: loadingState = .loaded
    @Published var satScores: SATScores?
    
    let schools_url_constant = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let sat_scores_url_constant = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    //MARK: Fetch data
    //these methods assume that the data set doesn't change..
    //for this solution, local cache is checked.. if no local cached, load from backend, then cache results.
    //future improvement would be to check backend for any data that doesn't already exist locally
    @MainActor
    func fetchSchools() async {
        loadingState = .loading
        var decodedHighschools = [HighSchool]()
        guard let url = URL(string: schools_url_constant) else {
            loadingState = .failed
            return
        }
        do {
            if let cachedData = getCachedResponse(forURL: url) {
                decodedHighschools = try JSONDecoder().decode([HighSchool].self, from: cachedData)
            } else {
                let (data, _) = try await URLSession.shared.data(from: url)
                decodedHighschools = try JSONDecoder().decode([HighSchool].self, from: data)
                //save to local data store for faster loading on next query
                self.cacheResponse(data: data, forURL: url)
            }
            
            self.schools = decodedHighschools
            self.loadingState = .loaded
        } catch {
            print("Error fetching or decoding highschool data: \(error)")
            loadingState = .failed
        }
    }
    
    @MainActor
    func fetchSatScores(_ school_name: String) async {
        loadingState = .loading
        var decodedSATScores = [SATScores]()
        guard let url = URL(string: sat_scores_url_constant) else {
            loadingState = .failed
            return
        }
        do {
            if let cachedData = getCachedResponse(forURL: url) {
                decodedSATScores = try JSONDecoder().decode([SATScores].self, from: cachedData)
            } else {
                let (data, _) = try await URLSession.shared.data(from: url)
                decodedSATScores = try JSONDecoder().decode([SATScores].self, from: data)
                //save to local data store for faster loading on next query
                self.cacheResponse(data: data, forURL: url)
            }
            
            //iterate through satScores and return satScores that belong to user selected school
            for satScore in decodedSATScores {
                guard let sat_school_name = satScore.school_name else {continue}
                if school_name.lowercased() == sat_school_name.lowercased() {
                    self.satScores = satScore
                    self.loadingState = .loaded
                    return
                }
            }
            self.loadingState = .failed
        } catch {
            print("Error fetching or decoding sat score data: \(error)")
            loadingState = .failed
        }
    }
    
    //Cache response data with a URL as unique key
    func cacheResponse(data: Data, forURL url: URL) {
        UserDefaults.standard.set(data, forKey: url.absoluteString)
    }

    //Get cached response data by URL
    func getCachedResponse(forURL url: URL) -> Data? {
        return UserDefaults.standard.data(forKey: url.absoluteString)
    }
}

//MARK: DATA MODELS
//Below methods define the data models for the highschools and sat scores
//These methods allow the data to be automatically decoded into their relevant data objects from the backend..'
//without needing to write boiler plate code
//also allows for easy refactoring in the case that requirements change in the frontend or backend

struct HighSchool: Codable, Identifiable {
    var id = UUID()
    var dbn: String?
    var school_name: String?
    var overview_paragraph: String?
    var phone_number: String?
    var school_email: String?
    var website: String?
    var primary_address_line_1: String?
    var city: String?
    var zip: String?
    var state_code: String?
    var latitude: String?
    var longitude: String?
    
    init(dbn: String? = nil, school_name: String? = nil, overview_paragraph: String? = nil, phone_number: String? = nil, school_email: String? = nil, website: String? = nil, primary_address_line_1: String? = nil, city: String? = nil, zip: String? = nil, state_code: String? = nil, latitude: String? = nil,
         longitude: String? = nil, satScores: SATScores? = nil) {
        self.dbn = dbn
        self.school_name = school_name
        self.overview_paragraph = overview_paragraph
        self.phone_number = phone_number
        self.school_email = school_email
        self.website = website
        self.primary_address_line_1 = primary_address_line_1
        self.city = city
        self.zip = zip
        self.state_code = state_code
        self.latitude = latitude
        self.longitude = longitude
    }
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case school_name
        case overview_paragraph
        case phone_number
        case school_email
        case website
        case primary_address_line_1
        case city
        case zip
        case state_code
        case latitude
        case longitude
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(dbn, forKey: .dbn)
        try container.encode(school_name, forKey: .school_name)
        try container.encode(overview_paragraph, forKey: .overview_paragraph)
        try container.encode(phone_number, forKey: .phone_number)
        try container.encode(school_email, forKey: .school_email)
        try container.encode(website, forKey: .website)
        try container.encode(primary_address_line_1, forKey: .primary_address_line_1)
        try container.encode(city, forKey: .city)
        try container.encode(zip, forKey: .zip)
        try container.encode(state_code, forKey: .state_code)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
    }
}

struct SATScores: Identifiable, Codable {
    var id = UUID()
    var dbn: String?
    var school_name: String?
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
    
    init(dbn: String? = nil, school_name: String? = nil, num_of_sat_test_takers: String? = nil, sat_critical_reading_avg_score: String? = nil,
         sat_math_avg_score: String? = nil, sat_writing_avg_score: String? = nil) {
        self.dbn = dbn
        self.school_name = school_name
        self.num_of_sat_test_takers = num_of_sat_test_takers
        self.sat_critical_reading_avg_score = sat_critical_reading_avg_score
        self.sat_math_avg_score = sat_math_avg_score
        self.sat_writing_avg_score = sat_writing_avg_score
    }
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case school_name
        case num_of_sat_test_takers
        case sat_critical_reading_avg_score
        case sat_math_avg_score
        case sat_writing_avg_score
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(dbn, forKey: .dbn)
        try container.encode(school_name, forKey: .school_name)
        try container.encode(num_of_sat_test_takers, forKey: .num_of_sat_test_takers)
        try container.encode(sat_critical_reading_avg_score, forKey: .sat_critical_reading_avg_score)
        try container.encode(sat_math_avg_score, forKey: .sat_math_avg_score)
        try container.encode(sat_writing_avg_score, forKey: .sat_writing_avg_score)
    }
}

//keeps track of data loading state for UI updates
enum loadingState {
    case loading
    case loaded
    case failed
}
